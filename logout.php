<?php
session_name('smart_gui');
session_start();
$_SESSION = array();
session_destroy();
header('Location: index.php');
die();
?>