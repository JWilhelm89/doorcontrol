<!DOCTYPE html>
<html>
<head>
<title>Grundriss</title>
<meta charset="utf-8">
<?php
global $rootdir;
echo "<link rel='shortcut icon' type='image/x-icon' href='".$rootdir."/img/icon/favicon.ico'>";
echo "<script src='".$rootdir."/scripts/jquery/jquery-3.6.0.min.js'></script>"; // Falls jquery extern bezogen werden soll
echo "<link rel='stylesheet' href='".$rootdir."/scripts/jquery-ui-1.13.1/jquery-ui.min.css'>";
echo "<script src='".$rootdir."/scripts/jquery-ui-1.13.1/jquery-ui.min.js'></script>";
?>
</head>
<body>