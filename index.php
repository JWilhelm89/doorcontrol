<?php
require_once 'core/init.php';
include 'include/header.php';
global $rootdir;
echo "<link rel='stylesheet' href='".$rootdir."/css/screen_common.css'>";
echo "<link rel='stylesheet' href='".$rootdir."/css/screen.css'>";
check_min_user();
if(empty($_POST['type']) === FALSE){
    
}else{
?>
<script>
var rooms = new Array();
var items = new Array();
var opc_data;

$( document ).ready(function() {
    /*** Optimierte Funktion zum zeitlich versetzten Neuladen des SVGs ***/
    var rtime;
    var timeout = false;
    var delta = 200;
    $(window).resize(function() {
        $('.svgbg').hide();
        rtime = new Date();
        if (timeout === false) {
            timeout = true;
            setTimeout(resizeend, delta);
        }
    });


    function resizeend() {
        if (new Date() - rtime < delta) {
            setTimeout(resizeend, delta);
        } else {
            timeout = false;
            const link = get_bg_size();
        }
    }

        
    const link = get_bg_size();
    get_OPC_Data();
});


function get_display_corr(){
    /*** Ermittelt das Darstellungs-Verhältnis des Grundrisses ***/
    var img = new Image();
    var $el = $('#floorplan');
    img.src = $el.css('background-image').replace(/url\(\"|\"\)$/ig, "");

    // Liefert die Bildhöhe und Bildbreite
    var bgImgWidth = img.width;
    var bgImgHeight = img.height;

    // Liefert das Umwandlungsverhältnis (rel) zur DIV-Größe
    var w = bgImgWidth / $el.width();
    var h = bgImgHeight / $el.height();
    var xoffset = 0;
    var yoffset = 0;
    if(w > h){
        var rel = 1/w;
        yoffset = Math.round(((window.innerHeight/2)) - ((bgImgHeight/(bgImgWidth/$el.width()))/2));
    }else{
        var rel = 1/h;
        xoffset = Math.round(((window.innerWidth/2)) - ((bgImgWidth/(bgImgHeight/$el.height()))/2));
    }
    return [rel, xoffset, yoffset];
}


function zoom_room(id){
    /*** Funktion um SVG zu zoomen ***/
    var arr = get_display_corr();
    $.ajax({
        url: 'ajax/get_room_border.php',
        type: 'POST',
        dataType:'json',
        async: false,
        data: {'id':id, 'rel':arr[0], 'x':arr[1], 'y':arr[2]},
        success: function(result) {
            $('.svgbg').attr('viewBox', `${result}`);
            $('#floorplan').css('background-position', '-9999px -9999px'); // Problem: Hintergrund zoomt nicht mit...
        }
    });
}


function get_bg_size(){
    var arr = get_display_corr();
    /*** Lädt die Räume ***/
    $.ajax({
        url: 'ajax/get_rooms.php',
        type: 'POST',
        dataType:'json',
        async: false,
        data: {'rel':arr[0], 'x':arr[1], 'y':arr[2]},
        success: function(roomarr) {
            globalThis.rooms = roomarr;
            /*** Lädt Türen ***/
            $.ajax({
                url: 'ajax/get_items.php',
                type: 'POST',
                dataType:'json',
                async: false,
                data: {'rel':arr[0], 'x':arr[1], 'y':arr[2]},
                success: function(itemsarr) {
                    globalThis.items = itemsarr;
                    get_OPC_Data(true);
                }
            });
        }
    });
    return arr;
}

function get_room_center(arr_string){
    var x = [];
    var y = [];
    var out = [];
    arr = arr_string.split(', ');
    
    $.each(arr, function(index, value){
        point = value.split(',');
        x.push(parseInt(point[0]));
        y.push(parseInt(point[1]));
    });

    var xmax = Math.max(...x);
    var ymax = Math.max(...y);
    var xmin = Math.min(...x);
    var ymin = Math.min(...y);
    var xoffset = Math.floor((xmax-xmin)/2);
    var yoffset = Math.floor((ymax-ymin)/2);
    out1 = (xmin + xoffset);
    out2 = (ymin + yoffset + 8);
    out.push(out1);
    out.push(out2);
    return out;
}


function startfunc(rooms, items='', opc_data=''){
    /*** Initialisierung / Anzeige der Räume ***/
    var dheight = $("#floorplan").height();
    var dwidth = $("#floorplan").width();
    $('.svgbg').remove();
    imgsrc = '<?php echo $rootdir.get_floorplan_path(); ?>';
    var html = "<svg width="+dwidth+" height="+dheight+" x='0px' y='0px' class='svgbg' viewbox='0 0 "+dwidth+" " +dheight+ "' style='overflow: hidden; margin:0; padding:0; float:left;'>";
    html += "<pattern id='img1' patternUnits='userSpaceOnUse' width='"+dwidth+"' height='"+dheight+"'><image href="+imgsrc+" x='0' y='0' width='"+dwidth+"' height='"+dheight+"' /></pattern>";
    html += "<rect style='fill:url(#img1) #fff' x='0' y='0' height='"+dheight+"' width='"+dwidth+"'></rect>";
    dataIn = opc_data;
    if(dataIn != ''){
    /*** Anzeige der Räume ***/
        for(i=0; i < rooms.length; i++){
            //if(generateRandom(2) == 2){ // Zufallsfunktion
            str = rooms[i][4];
            var index = -1;
            var val = str;
            var filteredObj = dataIn['variables'].find(function(item, b){
                if(item.name === val){
                    index = b;
                    if(dataIn.variables[index]['value'] == 'True'){
                        anim = "style='animation: blink 2s infinite ease-in;'"
                    }
                    return b;
                }else{
                    anim = "style='opacity: 0.01; stroke: none;'";
                }
            });
            html += "<polygon class='room "+rooms[i][0]+"' name='"+rooms[i][0]+"' points='"+rooms[i][5]+"' "+anim+" onclick=zoom_room('"+rooms[i][0]+"') opacity='0.2'/>";
            if(rooms[i][3] != ""){
                var filteredObj = dataIn['variables'].find(function(item, b){
                        index = b;
                    if(item.name === rooms[i][3]){
                        temperatur = parseInt(dataIn.variables[index]['value'])/10; // Der Wert der SPS muss durch 10 geteilt werden, da keine Dezimialstelle übertragen wird.
                        textcoords = get_room_center(rooms[i][5][0]); // Ermittelt die Mitte eines Raumes
                        html += "<text text-anchor='middle' x='"+textcoords[0]+"' y='"+textcoords[1]+"' class='temperatur'>"+temperatur+" °C</text>";
                        return b;
                    }
                });
            }
        }
    /*** Anzeige der Items ***/
        if(items !== ""){
            for(i=0; i < items.length; i++){
                var coordinates = items[i][4];
                const coords = coordinates.split(', ');
                coord1 = coords[0].split(","); // Punkt 1 (Startpunkt des Türblatts, gleichzeitig Drehachse)
                coord2 = coords[1].split(","); // Punkt 2 (Ende des Türblatts)
                
                if(items[i][3] != ''){
                    str = items[i][3];
                    var val = str;
                    if(items[i][2] == "tuer"){ // Tür-Darstellung
                        var appendix = "";
                        var color = "stroke: #3498eb; stroke-width: 1.5; stroke-linecap: butt;";
                        var filteredObj = dataIn['variables'].find(function(item, b){
                            if(item.name === val){
                                index = b;
                                if(dataIn.variables[index]['value'] == 'False'){
                                    color = "stroke: red; stroke-width: 1;";
                                    if(items[i][5] == "cw"){
                                        appendix = "transform='rotate(90, "+coord1[0]+", "+coord1[1]+")'";
                                    }else if(items[i][5] == "ccw"){
                                        appendix = "transform='rotate(270, "+coord1[0]+", "+coord1[1]+")'";
                                    }
                                }
                                return b;
                            }
                        });
                        html += "<line class='door "+items[i][0]+" line' x1='"+coord1[0]+"' y1='"+coord1[1]+"' x2='"+coord2[0]+"' y2='"+coord2[1]+"' "+appendix+" style='"+color+"'></line>"; // Ausgabe des Türblatts als Linie
                    }else if(items[i][2] == "stuer"){ // Schiebetür-Darstellung
                        var appendix = "";
                        var color = "stroke: #3498eb; stroke-width: 1.5; stroke-linecap: butt;";
                        var filteredObj = dataIn['variables'].find(function(item, b){
                            if(item.name === val){
                                index = b;
                                x = coord1[0]-coord2[0];
                                y = coord1[1]-coord2[1];
                                if(dataIn.variables[index]['value'] == 'False'){
                                    color = "stroke: red; stroke-width: 1;";
                                    appendix = "transform='translate("+x+" "+y+")'";
                                }
                                return b;
                            }
                        });
                        html += "<line class='door "+items[i][0]+" line' x1='"+coord1[0]+"' y1='"+coord1[1]+"' x2='"+coord2[0]+"' y2='"+coord2[1]+"' "+appendix+" style='"+color+"'></line>"; // Ausgabe des Türblatts als Linie
                    }
                }
            }
        }
    }
    html += "</svg>";
    $('#floorplan').append(html);
}


function get_OPC_Data(x){
/*** Die Funktion ruft die json Datei vom Python Script ab, welche zyklisch Daten vom OPC Server bezieht.
   * Der Parameter 'x' gibt an, ob die Funktion einmalig (x=true) oder zyklisch laufen soll. ***/
    $.ajax({
        url: "python/json_data.json",
        method: "GET",
        dataType: "json",
        async: false,
        cache: false,
        success: function(result) {
            opc_data = result;
            startfunc(rooms, items, opc_data=result);
            time = opc_data['time'];
        }
    }).then(function() {
        if(x==true){
        }else{
            setTimeout(get_OPC_Data, 10000);
        }
    });
}


/*** Schaltet das Bearbeitungs-Menü um (Mouse IN/OUT ***/
var isOnDiv = false;
$(document).on("mouseenter", '.admin_side-icon', function(){
        isOnDiv=true;
        check_admin_menu(isOnDiv);
    }
);
$(document).on("mouseleave", '.admin_menu', function(){
        isOnDiv=false;
        check_admin_menu(isOnDiv);
    }
);


function check_admin_menu(isOnDiv){
    if(isOnDiv == true){
        $('.admin_menu').show();
    }else{
        $('.admin_menu').hide();
    }
}

function show_login(){
    $('.admin_menu').html();
    $('.admin_menu').load('login.php');
}
</script>
<?php
if(isset($_GET['dm'])){
    $dm = $_GET['dm'];
    if($dm == '1'){
?>
<style>
/*    NUR FÜR DARKMODE  */
    #floorplan {
        filter: invert(100%);
        -webkit-filter: invert(100%);
        background-color: white;
    }
</style>
<?php
    }
}
/*** Startbildschirm ***/
   echo "<div id='floorplan' class='main' style='overflow: hidden;'></div>";
/*** Admin-Menü ***/
   if(logged_in() === true){
        echo "<div class='admin_menu'>";
        echo "<a href='admin/setup_floorplan.php'>Einrichter-Modus</a>";
        if(is_admin($user_data['user_id']) == 1){
            echo "<br><a href='admin/users.php'>Benutzer-Verwaltung</a>";
        }
        echo "<br><a href='logout.php'>Logout</a></div>";
   }else{
       echo "<div class='admin_menu'><span id='login_menu' onclick='show_login()'>Anmelden</span></div>";
   }
   echo "<div class='admin_side-icon'>";
   $color = 'blue';
   echo "<svg viewBox='0 0 100 100' width='30' height='30'>"
           . "<rect width='90' height='20' rx='10' fill=".$color."></rect>"
           . "<rect y='30' width='90' height='20' rx='10' fill=".$color."></rect>"
           . "<rect y='60' width='90' height='20' rx='10' fill=".$color."></rect>"
           . "</svg>";
   echo "</div>";
}
include 'include/footer.php';
?>
