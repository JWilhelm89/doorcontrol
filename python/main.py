#!/usr/bin/env python
import sched
import time
import json
from opcua import Client, ua
from datetime import datetime

# Server Daten
ip = "172.29.70.127"
port = "4840"

# Abfrage-Intervall (in s)
intervall = 10

# ####################################
# Keine Änderungen nach dieser Zeile!
# ####################################

max_exec = (60 / intervall) - 1
b = 0

if ip == "":
    ip = "127.0.0.1"

if port == "":
    port = "4840"

client = Client("opc.tcp://" + ip + ":" + port)
s = sched.scheduler(time.time, time.sleep)


def do_something(sc):
    global b
    global max_exec
    if b < max_exec:
        print("OPC Daten werden abgerufen...")
        now = datetime.now()
        date = now.strftime("%d.%m.%y, %H:%M:%S")
        datei = open('/volume1/web/sc/python/log.txt', 'w+')
        datei.write(date)
        getopcdata()
        b += 1
        s.enter(intervall, 1, do_something, (sc,))


# Beispiel Funktionen
def read_input_value(node_id):
    client_node = client.get_node(node_id)  # get node
    client_node_value = client_node.get_value()  # read node value
    print("Value of : " + str(client_node) + ' : ' + str(client_node_value))


def write_value_int(node_id, value):
    client_node = client.get_node(node_id)  # get node
    client_node_value = value
    client_node_dv = ua.DataValue(ua.Variant(client_node_value, ua.VariantType.Int16))
    client_node.set_value(client_node_dv)
    print("Value of : " + str(client_node) + ' : ' + str(client_node_value))


def write_value_bool(node_id, value):
    client_node = client.get_node(node_id)  # get node
    client_node_value = value
    client_node_dv = ua.DataValue(ua.Variant(client_node_value, ua.VariantType.Boolean))
    client_node.set_value(client_node_dv)
    print("Value of : " + str(client_node) + ' : ' + str(client_node_value))


def getopcdata():
    try:
        client.connect()

        root = client.get_root_node()

        # Testaufrufe
        # browse_recursive(root)
        # read_input_value('ns=4;i=6')  # Alarm
        # read_input_value('ns=4;i=5')  # Alarm-Reset
        # read_input_value('ns=4;i=4')  # Feuchtigkeit
        # read_input_value('ns=4;i=3')  # Temperatur

        # write_value_bool('ns=3;s=""', True)
        # write_value_int('ns=3;s=""', 25)

        # PLC_1-Node:
        # print(root.get_children()[0].get_children()[3])

        # OPC-UA-Node:
        # print(root.get_children()[0].get_children()[2].get_children()[0].get_children()[0].get_variables())

        # Datenabfrage
        current_time = str(root.get_children()[0].get_children()[0].get_children()[7].get_children()[1].get_value())
        namespaces = root.get_children()[0].get_children()[2].get_children()[0].get_children()[0].get_variables()
        odat = []
        for i in namespaces:
            opc_node_name = str(i.get_browse_name()).split(':', 1)[1][:-1]
            opc_node_name = opc_node_name.replace("-", "_")
            opc_node_value = str(i.get_value())
            odat.append((str(opc_node_name), str(opc_node_value)))

        values = [{"name": dat[0], "value": dat[1]} for dat in odat]
        output = {"variables": values, "time": current_time}

        # vor "variables" könnte man eventuell noch die Uhrzeit der PLC einlesen und so abgleichen,
        # ob die Daten noch "aktuell" sind. (Error-Handling im Frontend)

        # Debugging
        # print(output)
        # print("The type of object is: ", type(output))
        # stud_obj = json.loads(output)
        # print(stud_obj)

        with open('/volume1/web/sc/python/json_data.json', 'w') as outfile:
            json.dump(output, outfile, sort_keys=True, indent=4)

    finally:
        client.disconnect()


# Hauptfunktion
if __name__ == "__main__":
    s.enter(intervall, 1, do_something, (s,))
    s.run()
