<?php
require_once '../core/init.php';
include '../include/header.php';
global $rootdir;
#echo "<link rel='stylesheet' href='".$rootdir."/css/screen_common.css'>";
#echo "<link rel='stylesheet' href='".$rootdir."/css/screen.css'>";
?>
<style>
    body {
        font-family: calibri;
    }
    table {
        border: 1px solid black;
        border-spacing: 0px;
        border-collapse: collapse;
    }
    table td, th {
        padding-left: 3px;
        padding-right: 3px;
        border: 1px solid black;
        cursor: default;
    }
    
    a {
        color: black;
        text-decoration: none;
    }
    
    a:hover, input {
        cursor: pointer;
    }
    
    img { vertical-align: bottom; }
</style>
<?php
// Admin-Check
if(is_admin($user_data['user_id'])==1){
    global $link;
    echo "<div style='height: 24px;'><a href='".$rootdir."/index.php'><span style='color: blue;'>Zurück zur Betrachter-Seite</span></a></div>";
    echo "<form method='post'>";
    echo "<table name=*user_table*>";
    echo "<tr><th>Login</th><th>Status</th><th>Berechtigung</th><th>Aktionen</th></tr>";
    $sql = "SELECT user_id, username, active, admin FROM users ORDER BY admin DESC, username DESC";
    $abfrage = mysqli_query($link, $sql);
    while($row = mysqli_fetch_object($abfrage)) {
        $uid        = $row -> user_id;
        $username   = $row -> username;
        $active     = $row -> active;
        $admin      = $row -> admin;
        
        switch($active){
            case 0:
                $status = "<span style='color: red;'>nicht freigeschaltet</span>";
                $actdeact = "<a href='delete_user.php?user_id=".$uid."'><img src='".$rootdir."/img/icon/delete.gif' style='width: 16px; height: 16px;' title='Benutzer löschen'></a>&nbsp;&nbsp;";
                $actdeact .= "<a href='activate_user.php?user_id=".$uid."'><img src='".$rootdir."/img/icon/ok.gif' style='width: 16px; height: 16px;' title='Benutzer aktivieren'></a>";
                break;
            case 1:
                $status = "<span style='color: green;'>freigeschaltet</span>";
                $actdeact = "<a href='delete_user.php?user_id=".$uid."'><img src='".$rootdir."/img/icon/delete.gif' style='width: 16px; height: 16px;' title='Benutzer löschen'></a>";
                break;
        }
        switch($admin){
            case 0:
                $admin_input = "<a href='admin_user.php?user_id=".$uid."' title='Benutzer als Admin freischalten'><input type='checkbox' name='admin'>&nbsp;Admin</a>";
                break;
            case 1:
                $admin_input = "<input type='checkbox' name='admin' checked>&nbsp;Admin";
                break;
        }
        echo "<tr><td>".$username."</td><td>".$status."</td><td>".$admin_input."</td><td style='text-align: center;'>".$actdeact."</td></tr>";
    }
    #echo "<tr><td colspan='4' style='text-align: center;'><input type='submit' value='Einstellungen übernehmen'></td></tr>";
    echo "</table>";
    echo "</form>";
}else{
    redirect($rootdir."/index.php");
}
include '../include/footer.php';
?>