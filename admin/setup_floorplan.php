<?php
require_once '../core/init.php';
include '../include/header.php';
echo "<link rel='stylesheet' href='".$rootdir."/css/screen_common.css'>";
echo "<link rel='stylesheet' href='".$rootdir."/css/admin.css'>";
/*** PHPinfo ***/
#phpinfo(); // Debugging
if(logged_in() === false){
    redirect("../index.php");
    exit();
}
?>
<script>
    var rooms = new Array();
    var items = new Array();
    
    $( document ).ready(function() {
        var fp = '<?php if(get_floorplan_path() != ""){echo $rootdir.get_floorplan_path();}?>';
        $("#floorplan").css("background-image", "url("+fp+")");
        $("#floorplan").mousemove(function(e){
        /*** Anzeige der Maus-Koordinaten auf Grundriss ***/            
            var parentOffset = $(this).offset();
            var relX = Math.round(e.pageX - parentOffset.left);
            var relY = Math.round(e.pageY - parentOffset.top);
            $("#coordinates").html();
            $("#coordinates").html("<span>X:\xa0" + ("\xa0\xa0\xa0\xa0"+relX).slice(-4) + "\</span><span style='margin-left:20px;'>Y:\xa0" + ("\xa0\xa0\xa0\xa0"+relY).slice(-4)+"</span>");
        });
    
        function startfunc(rooms, items=''){
        /*** Initialisierung / Anzeige der Räume ***/
            var dheight = $("#floorplan").height()-20;
            var dwidth = $("#floorplan").width()-20;
            var html = "<svg width='"+dwidth+"px' height='"+dheight+"px' x='0px' y='0px' class='svgbg' style='overflow: hidden; margin:0; padding:0;'>";
            for(i=0; i < rooms.length; i++){
                //console.log(rooms[i]);
                html += "<a href='#' class='room link "+rooms[i][0]+"' onclick='output_data(\""+rooms[i][0]+"\")'><polygon class='room "+rooms[i][0]+"' name='"+rooms[i][1]+"' points='"+rooms[i][5]+"' style='fill:green; stroke:black; stroke-width:1; fill-rule:evenodd;' opacity='0.2'/></a>";
            }
            if(items != ""){
                for(i=0; i < items.length; i++){
                    /* Work in Progress */
                    if(items[i][2] == 'tuer' || items[i][2] == 'stuer'){
                        coordinates = items[i][4];
                        const coords = coordinates.split(', ');
                        coord1 = coords[0].split(",");
                        coord2 = coords[1].split(",");
                        x = coord1[0];
                        y = coord1[1];
                        adr = items[i][3];
                        html += "<a href='#' class='item link "+items[i][0]+"' onclick='";
                        if(items[i][2] == 'tuer'){
                            html += "door_coords_input";
                        }else if(items[i][2] == 'stuer'){
                            html += "gate_coords_input";
                        }
                        html += "(\""+items[i][0]+"\", \""+items[i][1]+ "\", \"" +adr+ "\",[\"" +coords[0]+ "\",\""+ coords[1] +"\"], \""+items[i][5]+"\")'>";
                        html += "<circle class='circle door "+items[i][0]+" room"+items[i][1]+"' cx='"+x+"' cy='"+y+"' r='5'></circle>";
                        html += "<line class='door "+items[i][0]+" line room"+items[i][1]+"' x1='"+coord1[0]+"' y1='"+coord1[1]+"' x2='"+coord2[0]+"' y2='"+coord2[1]+"' style='stroke: red; stroke-width: 2; stroke-linecap: butt;'></line>";
                        html += "</a>";
                    }
                }
                i = 0;
            }
            html += "</svg>";
            $('#floorplan').append(html);
        }

        $('#button_room').click(function(){
            delete pts;
            pts=null;
            new_room();
        });
        
        $('#button_room_reset').click(function(){
            room_reset();
            enable_room_links();
        });
        
        $('#button_room_reset').hide();
        
        $('#div_coords > input').keyup( function(){
            //console.log('Änderung');
        });

        rooms = <?php echo json_encode(get_rooms()); ?>; /* ACHTUNG: PHP-Funktion, NICHT Ajax-Call! */
        //console.log(rooms);
        items = <?php echo json_encode(get_items()); ?>; /* ACHTUNG: PHP-Funktion, NICHT Ajax-Call! */
        startfunc(rooms, items);
        abort = false;
        disabledlinks = false;
    });
    
    function disable_room_links(){
        /*** Deaktiviert alle Room-Links ***/
            $('.room.link').addClass('disabled');
            $('.room.link').hover(function(){
                $(this).css({"cursor":"default"});
            })
            disabledlinks = true;
        };
        
    function enable_room_links(){
    /*** (re-)aktiviert alle Room-Links ***/
        $('.link').removeClass('disabled');
        $('.link').hover(function(){
                $(this).css({"cursor":"pointer"});
            })
        disabledlinks = false;
    };
    
    function read_json(elementid, selvar=''){
        var jsonout = "";
        $.ajax({
            url: '../python/json_data.json',
            dataType: 'json',
            async: false,
            success: function(data) {

                jsonout += "<option disabled hidden selected>Bitte auswählen...</option>";
                jsonout += "<option value=''>nicht definiert</option>";
                jsonout += "<option disabled></option>";
                $.each( data['variables'], function( key, val ) {
                    //console.log(val['name']);
                    jsonout += "<option value='"+val['name']+"'";
                    if(selvar != "" && val['name'] == selvar){
                        jsonout += " selected";
                    }
                    jsonout += ">" + val['name'] + "</option>";
                });
                $('#'+elementid).append(jsonout);
            }
        });
    }
    
    function recalcroom(id){
    /*** Aktualisiert die Darstellung des Raums (des Polygons) mit entsprechenden title, nach einer manueller Koordinaten-Eingabe ***/
        if(id==""){
            id="new"; // Falls ein neuer Raum erstellt wird, nutze "new" als Room-Title
        }
        var coordinates="";
        var i=0;
        $('span.roomspan > input.coords').each(function(){
            coordinates += $(this).val();
            if(i % 2 === 0){
                coordinates += ",";
            }else{
                coordinates += ", ";
            }
            i+=1;
        });
        var pts = coordinates.slice(0, -2);
        $('polygon.room.'+id).attr("points", pts);
        document.getElementById("floorplan").innerHTML += "";
    };
    
    function show_item_menu(){
        $('#item_menu').show();
    };
    
    function add_door(type, room_id){
        /*** Erstellt die Koordinaten für eine (neu gezeichnete) Türe ***/
        $('#button_del_room').hide();
        $('#button_item').hide();
        $('#hidden_menu').html('');
        $('#item_menu').html(''); // Leere #item_menu
        
        var coordinates;
        var coord_nr = 0;
        var randnum = "0";
        $("#floorplan").click(function(e) {
            coord_nr = coord_nr+1;
            var parentOffset = $("#floorplan").offset();
            var x = Math.round(e.pageX - parentOffset.left);
            var y = Math.round(e.pageY - parentOffset.top);
            
            if(coord_nr <= 2){
                if((coordinates == null || coordinates == "") && x !== "" && y!== ""){
                    coordinates = x+","+y;
                    pts = [coordinates]; // Wichtig für die Koordinaten-Ausgabe im Editor-Bereich
                    html = "<circle class='door "+randnum+" circle room "+room_id+"' cx='"+x+"' cy='"+y+"' r='5' style='display: inline;'></circle>";
                }else if(x !== "" && y!== ""){
                    coordinates += ", "+x+","+y;
                    pts = coordinates.split(", "); // // Wichtig für die Koordinaten-Ausgabe im Editor-Bereich
                    coord1 = pts[0].split(","); // Punkt 1 (Startpunkt der Türe, gleichzeitig Drehachse)
                    coord2 = pts[1].split(","); // Punkt 2 (Endpunkt der Türe)
                    html += "<line class='door "+randnum+" line room "+room_id+"' x1='"+coord1[0]+"' y1='"+coord1[1]+"' x2='"+coord2[0]+"' y2='"+coord2[1]+"' style='display: inline; stroke: red; stroke-width: 2;'></line>"; // Ausgabe des Türblatts als Linie
                }
                removeElementsByClass("door "+randnum); // Entferne temporäre Darstellungen (z.B. Drehachse)
                $('.svgbg').append(html); // Zeichne die Türe (Drehachse + Türblatt)
                if(type == 'door'){
                    door_coords_input('', room_id, '', pts, false);
                }else{
                    gate_coords_input('', room_id, '', pts, false);
                }
                document.getElementById("floorplan").innerHTML += ""; // Gebe die neue Zeichnung der Türe aus
            }
        });
    };
    
    function recalcdoor(id){
    /*** Aktualisiert die Darstellung des Items (der Tür) mit entsprechenden title, nach einer manueller Koordinaten-Eingabe ***/
        if(id==""){
            id="0"; // Falls ein neues Item erstellt wird, nutze "0" als Item-ID
        }
        var coordinates="";
        var i=0;
        $('span.doorspan > input.coords').each(function(){
            coordinates += $(this).val();
            if(i % 2 === 0){
                coordinates += ",";
            }else{
                coordinates += ", ";
            }
            i+=1;
        });
        
        var pts = coordinates.slice(0, -2);
        pts = coordinates.split(", "); // // Wichtig für die Koordinaten-Ausgabe im Editor-Bereich
        coord1 = pts[0].split(","); // Punkt 1 (Startpunkt der Türe, gleichzeitig Drehachse)
        coord2 = pts[1].split(","); // Punkt 2 (Endpunkt der Türe)
        $('circle.door.'+id).attr({"cx": coord1[0], "cy": coord1[1]}); // Aktualisiere den Kreis (Achse)
        $('line.door.'+id+'.line').attr({"x1": coord1[0], "y1": coord1[1], "x2": coord2[0], "y2": coord2[1]}); // Aktualisiere die Linie
        document.getElementById("floorplan").innerHTML += ""; // Aktualisiere die Darstellung der Türe
    };
    
    function add_tor(room_id){
        $('#item_menu').html('');
        var out = ""; // Variable für Output
        //console.log("Raum-ID: " + room_id);
    }
    
    function coords_input(id, title, name, temp, alarm, arr){
        /*** Zeigt Eingabefelder mit Namen und den Koordinaten eines Raumes ***/
        $('#hidden_menu').html("");
        var i = 0;
        var out = "";
        var change = "oninput='recalcroom(\""+id+"\")'";
        disable_room_links();
        if(alarm == null){
            alarm = '';
        }
        if(temp == null){
            temp = '';
        }
        out += "<div class='coord_out'>";
        out += "<div><span class='headline'>Raum '"+name+"'</span> (ID: "+id+")</div><hr>";
        out += "<table style='width: 100%; margin-bottom: 20px;'>";
        out += "<tr><td>Titel:</td><td colspan='2'><input name='room_title' value='"+title+"' required></td></tr>";
        out += "<tr><td>Name:</td><td colspan='2'><input type='text' name='room_name' value='"+name+"' required></td></tr>";
        out += "<tr><td>Temperatur-Variable:</td><td colspan='2'><select id='temp_adr' name='temp_adr'></select></td></tr>";
        out += "<tr><td>Alarm-Variable:</td><td colspan='2'><select id='alarm_adr' name='alarm_adr'></select></td></tr>";
        $.each(arr, function(key, value){
            wert = value.split(",");
            xwert = wert[0];
            ywert = wert[1];
            out += "<tr><td>P"+(i+1)+":</td><td><span class='roomspan' style='margin-right: 10px; margin-left: 10px;'>x <input class='coords' name='x_"+i+"' type='number' value='"+xwert+"' "+change+" required></span></td>";
            out += "<td><span class='roomspan'>y <input class='coords' name='y_"+i+"' type='number' value='"+ywert+"' "+change+" required></span></td>";
            out += "</tr>";
            i = i+1;
        });
        out += "<tr><td colspan='3' style='text-align: center;'><input type='submit' class='button' id='acceptRoomBTN' value='eintragen'></td></tr></table>"
        out += "</div>";
        $('#hidden_menu').append("<div id='div_coords'><form method='post'><input name='type' type='hidden' value='add_room'><input type='hidden' name='id' value='"+id+"'>"+out+"</form></div>");
        $('#hidden_menu').append("<div id='item_menu'><input id='tuer_btn' class='button' type='button' value='Tür einfügen' onclick='add_door(\"door\","+id+")'><input id='tor_btn' class='button' type='button' value='Schiebetür einfügen' style='margin-left: 10px;' onclick='add_door(\"slide\","+id+")'><br><form name='item-form' method='post'></form></div>");
        $('#hidden_menu').show();
        read_json('temp_adr',temp);
        read_json('alarm_adr',alarm);
    }
    
    function door_coords_input(id, room_id, adr, arr, rotation){
        /*** Zeigt Eingabefelder einer Türe ***/
        var i = 0;
        var out = "";
        var change = "oninput='recalcdoor(\""+id+"\")'";
        $('#hidden_menu').html("");
        out += "<div class='door_coord_out'>";
        out += "<div><span class='headline'>Türe</span> (ID: "+id+")</div><hr>"
        out += "<table style='width: 100%; margin-bottom: 20px;'>";
        $.each(arr, function(key, value){
            wert = value.split(",");
            xwert = wert[0];
            ywert = wert[1];
            if(i == 0){
                pos = "start";
            }else{
                pos = "ende";
            }
            /* Ausgabe der Koordinaten */
            out += "<tr><td>"+pos[0].toUpperCase() + pos.slice(1) + ":</td>";
            out += "<td><span class='doorspan'>x <input class='coords' name='"+pos+"_x' type='number' value='"+xwert+"' "+change+" required></span></td>";
            out += "<td><span class='doorspan'>y <input class='coords' name='"+pos+"_y' type='number' value='"+ywert+"' "+change+" required></span></td></tr>";
            i = i+1;
        });
        if(i==2){
            out += "<tr><td>Öffnung:</td><td colspan='2' style='text-align: right;'><select name='door_rotation' required><option value='' disabled hidden>Bitte auswählen...</option><option value='ccw'";
            if(rotation == "ccw"){
                out += " selected";
            }
            out += ">gegen Uhrzeigersinn</option><option value='cw'";
            if(rotation == "cw"){
                out += " selected";
            }
            out += ">im Uhrzeigersinn</option></select></td></tr>";
        }
        if(adr == "null"){
            adr = "";
        }
        out += "<tr><td>SPS-Variable:</td>";
        out += "<td colspan='2' style='text-align: right;'><select id='item_adr' name='item_adr'>";        
        out += "</select>";
        out += "</td></tr>";
        out += "<tr><td colspan='3' style='text-align: center;'><input type='submit' class='button' value='übernehmen'></td></tr></table>";
        out +="</div>";
        /* Ausgabe Formular + Inhalt */
        $('#hidden_menu').append("<form method='post'><input name='type' type='hidden' value='add_door'><input name='room_id' type='hidden' value='"+room_id+"'><input name='id' type='hidden' value='"+id+"'>"+out+"</form>");
        $('#button_item.button').hide();
        if(id != "0"){
            /* Button "Item löschen" einblenden */
            $('#deleter').html("<a href='delete_item.php?del_id="+id+"' onclick='return confirm(\"Item ``"+id+"´´ wirklich löschen?\")'><button id='button_del_item' class='button'>Item löschen</button></a>");
        }
        read_json('item_adr', adr);
    }
    
    function gate_coords_input(id, room_id, adr, arr, rotation){
        /*** Zeigt Eingabefelder einer Schiebetüre ***/
        var i = 0;
        var out = "";
        var change = "oninput='recalcdoor(\""+id+"\")'";
        $('#hidden_menu').html("");
        out += "<div class='door_coord_out'>";
        out += "<div><span class='headline'>Schiebetüre</span> (ID: "+id+")</div><hr>"
        out += "<table style='width: 100%; margin-bottom: 20px;'>";
        $.each(arr, function(key, value){
            wert = value.split(",");
            xwert = wert[0];
            ywert = wert[1];
            if(i == 0){
                pos = "start";
            }else{
                pos = "ende";
            }
            /* Ausgabe der Koordinaten */
            out += "<tr><td>"+pos[0].toUpperCase() + pos.slice(1) + ":</td>";
            out += "<td><span class='doorspan'>x <input class='coords' name='"+pos+"_x' type='number' value='"+xwert+"' "+change+" required></span></td>";
            out += "<td><span class='doorspan'>y <input class='coords' name='"+pos+"_y' type='number' value='"+ywert+"' "+change+" required></span></td></tr>";
            i = i+1;
        });
        if(i==2){
            out += "<input type='hidden' name='door_rotation' value='slide'>";
        }
        if(adr == "null"){
            adr = "";
        }
        out += "<tr><td>SPS-Variable:</td>";
        out += "<td colspan='2' style='text-align: right;'><select id='item_adr' name='item_adr'>";
        out += "</select>";
        out += "</td></tr>";
        out += "<tr><td colspan='3' style='text-align: center;'><input type='submit' class='button' value='übernehmen'></td></tr></table>";
        out +="</div>";
        /* Ausgabe Formular + Inhalt */
        $('#hidden_menu').append("<form method='post'><input name='type' type='hidden' value='add_sdoor'><input name='room_id' type='hidden' value='"+room_id+"'><input name='id' type='hidden' value='"+id+"'>"+out+"</form>");
        $('#button_item.button').hide();
        if(id != "0"){
            /* Button "Item löschen" einblenden */
            $('#deleter').html("<a href='delete_item.php?del_id="+id+"' onclick='return confirm(\"Item ``"+id+"´´ wirklich löschen?\")'><button id='button_del_item' class='button'>Item löschen</button></a>");
        }
        read_json('item_adr', adr);
    }
    
    function hide_coords_input(){
    /*** Versteckt das Menü mit den Raum-Koordinaten ***/
        $('#hidden_menu').html("");
        $('#hidden_menu').hide();
    }
    
    function edit_polygon(data){
        var id = data[0];
        var title = data[1];
        var r_name = data[2];
        var temp = data[3];
        var alarm = data[4];
        var geometry_arr = data[5].split(", ");
        coords_input(id, title, r_name, temp, alarm, geometry_arr);
        $('#acceptRoomBTN').prop("value", "Änderungen übernehmen");
    }
    
    function output_data(data){
    /*** Holt sich die Daten zu einem Raum mittels Raum-Title ***/
        if(disabledlinks === true){
            return false; // verhindert die Auswahl eines bestehenden Raumes während ein neuer Raum erstellt oder bearbeitet wird
        }
        var room_title = 'id=' + data;
        $.ajax({ 
            type: "POST", 
            url: "../ajax/room_data.php",
            data: room_title,
            success: function(msg){
                var obj = $.parseJSON(msg);
                edit_polygon(obj);
                
                $('#button_room').hide();
                
                /* Button "abbrechen" einblenden */
                    $('#button_room_reset').show();
                    
                /* Button "Raum löschen" einblenden */
                    $('#deleter').html("<a href='delete_room.php?del_id="+obj[0]+"' onclick='return confirm(\"Raum ``"+obj[2]+"´´ wirklich löschen?\")'><button id='button_del_room' class='button'>Raum löschen</button></a>");
                
                /* [TBD] Button um Items (Türen, Tore) hinzufügen */
                    $('#insert_items').html("<input id='button_item' class='button' type='button' value='Objekt einfügen' style='margin-left: 10px;' onclick='show_item_menu()' title='Objekte können Türen oder Tore sein.\nBeide Arten werden im geschlossenen Zustand wir folgt gezeichnet.\n\nBei Türen wird durch den ersten Mausklick der Drehpunkt (das Scharnier) definiert, der zweite Klick definiert die Länge des Türblatts.\n\nBei Schiebetoren verhält sich dies ähnlich: der erste Klick definiert den Punkt um den sich das Tor herum schließt. Der zweite Punkt ist der Punkt im Anschlag des Tores, wenn es geschlossen ist (z.B. der Torrahmen).\n\nDer Dreh- oder Verschiebe-Punkt wird durch einen Punkt dargestellt, dem die Länge des Tores / des Türblatts als Linie folgt.'>");
                    
                /* Items des Raumes anzeigen */
                    $('.room'+obj[0]).show();
            }
        });
    }
    
    function removeElementsByClass(className){
    /*** (Hilfs-) Funktion zum Löschen von Classes ***/
        const elements = document.getElementsByClassName(className);
        while(elements.length > 0){
            elements[0].parentNode.removeChild(elements[0]);
        }
    }
    
    function room_reset(){
    /*** Funktion um einen Raum (Polygon) zu löschen ***/
        abort = true;
        $('.room').remove();
        var pts = null;
        abort = false;
        location.reload();
    }
    
    function new_room(){
    /*** Funktion zur Erstellung eines neuen Raumes (Polygons) ***/
        $('#button_room').hide();
        $('#button_room_reset').show();
        disable_room_links();
        
        var randnum = "new"; // Fester Wert als für die eigentliche random-Funktion        
        
        $("#floorplan").click(function(e) {
            var parentOffset = $("#floorplan").offset();
            var x = Math.round(e.pageX - parentOffset.left);
            var y = Math.round(e.pageY - parentOffset.top);

            if(pts === null && x !== "" && y!== ""){
                pts = x+","+y;
            }else if(x !== "" && y!== ""){
                pts += ", "+x+","+y;
            }

            // Zur Aktualisierung des Polygons, wird das bestehende gelöscht und mit den neuen Daten neu erzeugt.
            removeElementsByClass("room "+randnum);
            
            // Spätere Ausgabe der Koordinaten
            punkte = pts.split(", ");
            coords_input('', '', '', '', '', punkte);
            
            // Darstellung des Polygons
            var html = "<polygon class='room "+randnum+"' points='"+pts+"' style='fill:green; stroke:black; stroke-width:1; fill-rule:evenodd;' opacity='0.2'>";
            $('.svgbg').append(html);
            document.getElementById("floorplan").innerHTML += "";
        });
    }

// Funktionen für den Bildupload
function preview_image(event) {
    var reader = new FileReader();
    reader.onload = function(){
            if($('#anhang')[0].files[0].size > 10485760){
            alert("Die ausgewählte Datei ist zu groß für den Upload!");
            bildloeschen();
        }else{
            picSize = returnFileSize($('#anhang')[0].files[0].size);
            var div = document.getElementById('pic_sel');
            div.innerHTML = "<input type='submit' value='jetzt hochladen'><span style='margin-left: 15px;'>Quelldatei-Größe: " + picSize + "</span><span style='padding-left: 10px;'><a href='#' style='font-style: normal;' onclick='bildloeschen();'>[ <img src='../img/icon/delete.gif' style='width: 16px; height: 16px;'> löschen ]</a></span>"
        }
    }
    reader.readAsDataURL(event.target.files[0]);
}

function returnFileSize(number) {
    if(number < 1024) {
        return number + ' bytes';
    } else if(number >= 1024 && number < 1048576) {
        return (number/1024).toFixed(1) + ' KB';
    } else if(number >= 1048576) {
        return (number/1048576).toFixed(1) + ' MB';
    }
}

function bildloeschen(){
    document.getElementById("anhang").value = "";
    $('#anhang')[0].value = "";
    document.getElementById('pic_sel').innerHTML = "";
}
</script>
<?php
if(empty($_POST['type']) === FALSE){
/*** übermittelte Formular-Daten ***/
    /*** MySQL-Anweisung um Raum in Datenbank einzutragen ***/
    if($_POST['type'] == 'add_room'){
        $room_title     = $_POST['room_title'];
        $room_name      = $_POST['room_name'];
        $new_coord_str  = "";
        $prev_coord_str = "";
        for($i = 0; $i < 100; $i++){
            if(isset($_POST["x_".$i]) && isset($_POST["y_".$i])){
                if($prev_coord_str != $_POST["x_".$i].",".$_POST["y_".$i]){ // Prüft ob der Punkt identisch ist mit vorherigen (dient dem altnativen Löschen von Punkten welche zu viel erstellt wurden)
                    if($new_coord_str != ""){
                        $new_coord_str = $new_coord_str . ", ";
                    }
                    $prev_coord_str = $_POST["x_".$i].",".$_POST["y_".$i];
                    $new_coord_str = $new_coord_str . $_POST["x_".$i].",".$_POST["y_".$i];
                }
            }else{
                $i = 100;
            }
        }
        if(isset($_POST['temp_adr']) && $_POST['temp_adr'] != ""){
            $temp_adr = "'".$_POST['temp_adr']."'";
        }else{
            $temp_adr = "NULL";
        }
        
        if(isset($_POST['alarm_adr']) && $_POST['alarm_adr'] != ""){
            $alarm_adr = "'".$_POST['alarm_adr']."'";
        }else{
            $alarm_adr = "NULL";
        }
        
        if(isset($_POST['id']) && $_POST['id'] != ""){
            $id = "'".$_POST['id']."'";
        }else{
            $id = "NULL";
        }
        
        $insert_values_string = "(id, title, name, geometry, temp_adr, alarm_adr) VALUES ($id,'".$room_title."', '".$room_name."', '".$new_coord_str."', $temp_adr, $alarm_adr)";
        $update_values_string = "UPDATE title='".$room_title."', name='".$room_name."', geometry='".$new_coord_str."', temp_adr=$temp_adr, alarm_adr=$alarm_adr";
        
        $sql = "INSERT INTO rooms ".$insert_values_string." ON DUPLICATE KEY ".$update_values_string;
        mysqli_query($link, $sql) or die(var_dump($sql."<br>".mysqli_error($link)));
        redirect('../index.php');
        exit();
    }

    /*** MySQL-Anweisung um Türe in Datenbank einzutragen ***/
    if($_POST['type'] == 'add_door' || $_POST['type'] == 'add_sdoor'){
        $room_id        = $_POST['room_id'];
        $start          = $_POST['start_x'].",".$_POST['start_y'];
        $ende           = $_POST['ende_x'].",".$_POST['ende_y'];
        $coords         = $start.", ".$ende;
        $door_rotation  = $_POST['door_rotation'];
        
        if($_POST['type'] == 'add_door'){
            $type       = "tuer";
        }else{
            $type       = "stuer";
        }
        
        if(isset($_POST['id']) && $_POST['id'] != ""){
            $id = "'".$_POST['id']."'";
        }else{
            $id = "NULL";
        }
        
        if(isset($_POST['item_adr']) && $_POST['item_adr'] != ""){
            $item_adr = "'".$_POST['item_adr']."'";
        }else{
            $item_adr = "NULL";
        }
        
        $insert_values_string = "(id, room_id, item_adr, type, geometry, rotation) VALUES ($id, '".$room_id."', $item_adr, '".$type."', '".$coords."', '".$door_rotation."')";
        $update_values_string = "UPDATE room_id='".$room_id."', item_adr=$item_adr, type='".$type."', geometry='".$coords."', rotation='".$door_rotation."'";
        $sql = "INSERT INTO items ".$insert_values_string." ON DUPLICATE KEY ".$update_values_string;
        mysqli_query($link, $sql) or die(var_dump($sql."<br>".mysqli_error($link)));
        redirect('../index.php');
        exit();
    }
/*** Bild- / Grundriss-Upload ***/
    if($_POST['type'] == "grundriss" && isset($_FILES['anhang']) && !empty($_FILES['anhang']) && $_FILES['anhang']['size'] != '0'){
        global $rootdir;
        $upload_folder = $_SERVER["DOCUMENT_ROOT"] . $rootdir . "/upload/";
        $filename = pathinfo($_FILES['anhang']['name'], PATHINFO_FILENAME);

        $extension = strtolower(pathinfo($_FILES['anhang']['name'], PATHINFO_EXTENSION));
        $allowed_extensions = array('png', 'jpg', 'jpeg', 'gif');
        if(!in_array($extension, $allowed_extensions)) {
            die("Ungültige Dateiendung. Nur png, jpg und jpeg-Dateien sind erlaubt");
        }
        $max_size = 10*1024*1024; //10 MB
        if($_FILES['anhang']['size'] > $max_size) {
            die("Bitte keine Dateien größer 10 MB hochladen");
        }
        // Überprüfung ob tatsächlich ein Bild-Typ hochgeladen wird
        if(function_exists('exif_imagetype')){ //Die exif_imagetype-Funktion erfordert die exif-Erweiterung auf dem Server
            $allowed_types = array(IMAGETYPE_PNG, IMAGETYPE_JPEG);
            $detected_type = exif_imagetype($_FILES['anhang']['tmp_name']);
            if(!in_array($detected_type, $allowed_types)){
                die("Nur der Upload von JPG und PNG Bilddateien wird gestattet");
            }
        }
        $new_tmp_path = $upload_folder.$filename.'.'.$extension;
        move_uploaded_file($_FILES['anhang']['tmp_name'], $new_tmp_path);
        $pathstring = "/upload/" . $filename . '.' . $extension;
        set_floorplan_path($pathstring);
    }
    
}else{
/*** Startbildschirm ***/
    echo "<div class='grid-container'>";
    echo "<form name='grundriss_upload' action='' method='post' enctype='multipart/form-data'>";
    echo "<div class='obj_container'>";
        echo "<span style=''><a href='../index.php'>Zum Startbildschirm</a></span>";
        
/*** Container zum Austausch des Grundrisses ***/
        echo "<div style='display: inline; margin-left: 40px;'>";
            echo "<label for='anhang' style='font-weight: bold; font-style: italic; padding-right: 15px;'>neuen Grundriss hochladen <img src='../img/icon/help.gif' style='width: 12px; height: 12px;' title='Es empfiehlt sich einen möglichst hochauflösenden Grundriss hochzuladen, sobald viele Räume dargestellt werden sollen.\nDie Datei darf dabei nicht größer als 10 MB sein.'></label>";
            echo "<input type='hidden' name='type' value='grundriss'>";
            echo "<input id='anhang' name='anhang' type='file' accept='image/x-png, image/gif, .jpg, .jpeg' onchange='preview_image(event)'>";
            echo "<span id='pic_sel' style='margin-left: 10px; font-style: italic;'></span>";
        echo "</div>";
    echo "</div>";
    echo "</form>";

    # Grundriss-Overview
/*** OPTIMIERUNGSBEDARF ***/
    $floorplan = $_SERVER['DOCUMENT_ROOT'].$rootdir.get_floorplan_path();
    list($width, $height, $type, $attr) = getimagesize($floorplan);
    echo "<div class='floorplan_container'><div id='floorplan' style='width: ".($width+20)."px; height: ".($height+20)."px;'>";
    echo "</div>";
    echo "</div>";
    echo "<div id='coordinates'></div>";

    echo "<div class='sidebar_container'>";
    echo "<div id='hidden_menu'></div>";
    echo "<center><input id='button_room' class='button' type='button' value='Raum einfügen' style='margin-left: 10px;' title='Durch jeden einzelnen Mausklick werden Eckpunkte eines Raumes definiert.\nHierbei ist wichtig, dass die Reihenfolge wahlweise im Urzeigersinn oder gegen den Urzeigersinn erfolgt,nicht jedoch gemischt wird.'><span id='deleter'></span><span id='insert_items'></span><input id='button_room_reset' class='button' type='button' value='abbrechen' style='margin-left: 10px;'>";
    echo "</center>";
    echo "</div>";
    
# Flexbox Ende
    echo "</div>";
}
include '../include/footer.php';
?>