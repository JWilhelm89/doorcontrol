<?php
require_once '../core/init.php';
include '../include/header.php';
global $rootdir;

if(is_admin($user_data['user_id']) == 1){
    if(isset($_GET['del_id'])){
        function remove_item($item_id=''){
        /*** Funktion zum Löschen eines Items ***/    
            if(is_null($item_id)){
                return false;
            }else{
                global $link;
                /*** Item löschen ***/
                $sql = "DELETE FROM items WHERE id = '".$item_id."'";
                mysqli_query($link, $sql) or die(mysqli_error($link));
                unset($sql);
                return true;
            }
        }
        
        $id = $_GET['del_id'];
        if(remove_item($id) === true){
            redirect($rootdir.'/admin/setup_floorplan.php');
        }else{
            echo "<div style='margin-top:20%; text-align: center; color: red;'>Es ist leider ein Fehler aufgetreten.<br><a href='".$rootdir."/admin/setup_floorplan.php'>[ Zurück zum Bearbeiter-Bereich ]</div>";
        }
    }else{
        goto end;
    }
}else{
    end:
    redirect($rootdir.'/index.php');
}