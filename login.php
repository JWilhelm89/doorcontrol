<?php
include_once 'core/init.php';

if(empty($_POST) === FALSE){
    $username = $_POST['username'];
    $password = $_POST['password'];
    
    if(empty($username) === TRUE || empty($password) === TRUE){
        $errors[] = "Sie müssen einen Benutzernamen und Passwort eingeben.";
    } elseif(user_exists($username) === FALSE){
        $errors[] = "Der eingegebene Benutzer konnte nicht gefunden werden.";
    } elseif(user_active($username) === FALSE){
        $errors[] = "Ihr Zugang wurde noch nicht freigeschaltet.<br>Bitte warten Sie bis eine Freischaltung erfolgt ist.";
    } else {
        $login = login($username, $password);
        if($login === FALSE){
            $errors[] = "Die Kombination aus Benutzernam / Passwort ist nicht gültig.";
        }else{
            $_SESSION['user_id'] = $login;
            $user_id = user_id_from_username($username);
            redirect('admin/setup_floorplan.php');
            exit();
        }
    }
}
echo "<div class='login-box-frame'>"
            . "<div class='login-box' style='margin: 0 auto; display: table; text-align: center;'>"
                . "<form autocomplete='off' action='login.php' method='POST'>"
                    . "<div class='textbox'>"
                        . "<input type='text' placeholder='Benutzername' name='username' autofocus>"
                    . "</div>"
                    . "<div class='textbox'>"
                        . "<input type='password' placeholder='Passwort' name='password'>"
                    . "</div>"
                    . "<input type='submit' id='login' value='anmelden'><br>"
                    . "<a href='' onclick=\"window.open('register.php', '_blank', 'location=yes,height=570,width=520,scrollbars=yes,status=yes');\">registrieren</a>"
                . "</form>"
            . "</div>"
        . "</div>";

if(empty($errors) === FALSE){
    echo "<div class='error-msg'>";
    echo "<h2><font color=red><u><b>Fehler:</b></u></font></h2>";
    echo output_errors($errors);
    echo "</div>";
}
?>