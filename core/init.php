<?php
$rootdir = "/" . substr($_SERVER['SCRIPT_NAME'],1,strpos(substr($_SERVER['SCRIPT_NAME'], 1), '/'));
$path = '.:/volume1' . $rootdir;
set_include_path(get_include_path() . PATH_SEPARATOR . $path);

require_once 'database/connect.php';
require_once 'functions/general.php';
include 'initial_setup.php';

# Session-Einstellung
if(!isset($_SESSION)){
    session_name('smart_gui');
    session_start();
}

if(logged_in() == TRUE){
    $session_user_id = $_SESSION['user_id'];
    $user_data = user_info($session_user_id);
    if(user_active($user_data['username']) === false) {
        session_destroy();
    }
}

$errors = array(); # Für Fehlerausgaben auf der Seite
?>