<?php
# Ersatz für veraltetes "mysql_result"
function mysqli_result($res, $row, $field=0) {
    $res->data_seek($row);
    $datarow = $res->fetch_array();
    if(isset($datarow[$field])){
        return $datarow[$field];
    }
}

# Funktion zum Prüfen und automatischen Erstellen von Datenbank-Tables
function check_db(){
    global $link;
    $db = "smart_gui";
    $tables = array("rooms","items","users", "config");
    $sql_rooms = "CREATE TABLE `".$db."`.`rooms` ( `id` INT NOT NULL AUTO_INCREMENT, `title` VARCHAR(32) NOT NULL, `name` VARCHAR(32) NOT NULL, `temp_adr` VARCHAR(32) NULL DEFAULT NULL, `alarm_adr` VARCHAR(32) NULL DEFAULT NULL, `geometry` VARCHAR(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE = InnoDB;";
    $sql_items = "CREATE TABLE `".$db."`.`items` ( `id` INT NOT NULL AUTO_INCREMENT, `room_id` INT NOT NULL, `item_adr` VARCHAR(32) NULL DEFAULT NULL, `type` VARCHAR(32) NOT NULL, `geometry` VARCHAR(255) NOT NULL, `rotation` VARCHAR(32) NOT NULL, PRIMARY KEY (`id`)) ENGINE = InnoDB;";
    $sql_users = "CREATE TABLE `".$db."`.`users` ( `user_id` INT NOT NULL AUTO_INCREMENT , `username` VARCHAR(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL , `password` VARCHAR(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL , `active` TINYINT(1) NOT NULL DEFAULT '0' , `admin` TINYINT(1) NOT NULL DEFAULT '0', PRIMARY KEY (`user_id`)) ENGINE = InnoDB;";
    $sql_config = "CREATE TABLE `".$db."`.`config` (`id` INT NOT NULL AUTO_INCREMENT, `name` VARCHAR(32) NOT NULL, `value` VARCHAR(128) NOT NULL, PRIMARY KEY (`id`)) ENGINE = InnoDB;";
    $sql = "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '".$db."'";
    $db_selected = mysqli_query($link, $sql);
    if(mysqli_num_rows($db_selected) == 1){
        $i = 0;
        foreach ($tables as $table){
            if ($result = mysqli_query($link, "SHOW TABLES LIKE '".$table."'")) {
                $nums = mysqli_num_rows($result);
                if($nums == 1) {
                    # echo "Table exists";
                } else {
                    # echo "Datenbank-Table ".$table." ist nicht vorhanden.";
                    mysqli_query($link, ${"sql_".$table}) or die(mysqli_error($link));
                }
                if($table == "users") {
                    check_min_user();
                }
                $i = $i+1;
            }
        }
        if($i == count($tables)){
            remove_db_check();
        }
    }else{
        die("Datenbank `".$db."` nicht vorhanden!");
    }
}

# Funktion für redirects, da diese flexibler als header(Location: xy) Aufrufe ist
function redirect($url){
    if (!headers_sent())
    {    
        header('Location: '.$url);
        exit;
    } else {  
        echo '<script type="text/javascript">';
        echo 'window.location.href="'.$url.'";';
        echo '</script>';
        echo '<noscript>';
        echo '<meta http-equiv="refresh" content="0;url='.$url.'" />';
        echo '</noscript>';
        exit;
    }
}


# Registriere einen Benutzer
function register_user($register_data){
    global $link;
    array_walk($register_data, 'array_sanitize');
    $register_data['password'] = md5($register_data['password']);
    $fields = '`' . implode('`, `', array_keys($register_data)) . '`';
    $data = '\'' . implode('\', \'', $register_data) . '\'';
    mysqli_query($link, "INSERT INTO `users` ($fields) VALUES ($data)") or die(mysqli_error($link));
}

# Prüft, dass mindestens ein Administrator als User verfügbar ist, um sich vom System nicht aussperren zu können
function check_min_user() {
    global $link;
    if (mysqli_num_rows(mysqli_query($link, "SELECT user_id FROM users")) < 1){
        # Erstelle den Benutzer "Admin"
        mysqli_query($link, "INSERT INTO `users` (`user_id`, `username`, `password`, `active`, `admin`) VALUES (NULL, 'Admin', 'e3afed0047b08059d0fada10f400c1e5', '1', '1');");
    }
}


# Siehe sanitize, nur entsprechend für Arrays
function array_sanitize($item) {
    global $link;
    $item = mysqli_real_escape_string($link, $item);
}


# Deaktiviert die Kontrolle auf Vorhandensein von Datenbank-Tables
function remove_db_check(){
    global $rootdir;
    $path_to_file = $_SERVER['DOCUMENT_ROOT'] . $rootdir.'/core/initial_setup.php';
    #$perm = substr(sprintf('%o', fileperms($path_to_file)), -4)+0;
    #chmod($path_to_file, 0777);
    $file_contents = file_get_contents($path_to_file);
    $file_contents = str_replace("check_db();", "#check_db();", $file_contents);
    file_put_contents($path_to_file,$file_contents);
    #chmod($path_to_file, $perm);
    $new_path = str_replace((PATH_SEPARATOR.$path_to_file), '', get_include_path());
    set_include_path($new_path);
    return 1;
}


# Lädt alle Räume aus der Datenbank
function get_rooms($modX='', $modY='', $x='', $y=''){
    global $link;
    $sql = "SELECT * FROM rooms";
    $result = mysqli_query($link, $sql) or die(mysqli_error($link));
    if(mysqli_num_rows($result) >= 1){
        $rooms = array();
        while ($row = mysqli_fetch_object($result)) {
            $id         = $row -> id;
            $title      = $row -> title;
            $name       = $row -> name;
            $temp_adr   = $row -> temp_adr;
            $alarm_adr  = $row -> alarm_adr;
            $geometry   = $row -> geometry;
            $rooms[] = array($id, $title, $name, $temp_adr, $alarm_adr, $geometry);
        }
        #return $rooms;
        $output_arr = array();
        foreach ($rooms as $room){
            if($modX != "" || $modY != ""){
                $string = $room[5];
                $coordinate = explode(", ", $string);
                foreach($coordinate as $axis){
                    $value = explode(",",$axis);
                    $xval = round(($value[0])*($modX)+($x));
                    $yval = round(($value[1])*($modY)+($y));
                    $newcord = $xval.",".$yval;
                    $output[] = $newcord;
                    unset($value);
                }
                unset($coordinate);
                $out = implode($output,", ");
                unset($output);
                $output_arr[] = array($room[0], $room[1], $room[2], $room[3], $room[4], array($out));
            }else{
                $output_arr[] = array($room[0], $room[1], $room[2], $room[3], $room[4], array($room[5]));
            }
        }
        return array_values($output_arr);
    }else{
        return FALSE;
    }
}


# Lädt alle Türen aus der Datenbank
function get_items($modX='', $modY='', $x='', $y=''){
    global $link;
    $sql = "SELECT * FROM items";
    $result = mysqli_query($link, $sql) or die(mysqli_error($link));
    if(mysqli_num_rows($result) >= 1){
        $items = array();
        while ($row = mysqli_fetch_object($result)) {
            $id         = $row -> id;
            $room_id    = $row -> room_id;
            $type       = $row -> type;
            $adr        = $row -> item_adr;
            $geometry   = $row -> geometry;
            $rotation   = $row -> rotation;
            $items[] = array($id, $room_id, $type, $adr, $geometry, $rotation);
        }
        $output_arr = array();
        foreach ($items as $item){
            if($modX != "" || $modY != ""){
                $string = $item[3];
                $coordinate = explode(", ", $string);
                foreach($coordinate as $axis){
                    $value = explode(",",$axis);
                    $xval = round(($value[0])*($modX)+($x));
                    $yval = round(($value[1])*($modY)+($y));
                    $newcord = $xval.",".$yval;
                    $output[] = $newcord;
                    unset($value);
                }
                unset($coordinate);
                $out = implode($output,", ");
                unset($output);
                $output_arr[] = array($item[0],$item[1], $item[2], $out, $item[4], $item[5]);
            }else{
                $output_arr[] = array($item[0],$item[1], $item[2], $item[3], $item[4], $item[5]);
            }
        }
        return array_values($output_arr);
    } else {
        return FALSE;
    }
}


# Fehlerausgabe
function output_errors($errors) {
    return "<ul><li>" . implode("</li><li>", $errors) . "</li></ul>";
}


# Melde Benutzer an
function login($username, $password) {
    global $link;
    $user_id = user_id_from_username($username);

    $username = sanitize($username);
    $password = md5($password);

    return (mysqli_result(mysqli_query($link, "SELECT COUNT(`user_id`) FROM `users` WHERE `username` = '$username' AND `password` = '$password'"), 0) == 1) ? $user_id : FALSE;
}


# Prüfe ob Benutzer angemeldet ist
function logged_in() {
    return (isset($_SESSION['user_id'])) ? true : false;
}


# Weiterleitung falls Benutzer eingeloggt ist
function logged_in_redirect() {
    if (logged_in() === TRUE) {
        redirect('index.php');
        exit();
    }
}


# Die Funktion schreibt den Dateipfad zum Grundriss in die Datenbank
function set_floorplan_path($string){
    global $link;
    $string = sanitize($string);
    $id = (mysqli_result(mysqli_query($link, "SELECT id FROM config WHERE name='Grundriss'"), 0));
    if($id != false){
        $sql = "UPDATE config SET name = 'Grundriss', value = '".$string."' WHERE id='".$id."'";
        mysqli_query($link, $sql) or die(mysqli_error($link));
    }else{
        mysqli_query($link, "INSERT INTO config (name, value) VALUES ('Grundriss', '".$string."')") or die(mysqli_error($link));
    }
    mysqli_query($link, "TRUNCATE TABLE rooms") or die(mysqli_error($link));
    mysqli_query($link, "TRUNCATE TABLE items") or die(mysqli_error($link));
}


# Die Funktion holt den Dateipfad zum Grundriss aus der Datenbank
function get_floorplan_path(){
    global $link;
    global $rootdir;
    return (mysqli_result(mysqli_query($link, "SELECT value FROM config WHERE name='Grundriss'"), 0));
}


# Funktion zum 'escapen' von Sonderzeichen etc. ('Schutz' gegen SQL Injection Versuche)
function sanitize($data){
    global $link;
    return mysqli_real_escape_string($link, $data);
}


# Prüfe ob der Benutzer $username freigeschaltet ist
function user_active($username) {
    global $link;
    $username = sanitize($username);
    return (mysqli_result(mysqli_query($link, "SELECT COUNT(`user_id`) FROM `users` WHERE `username` = '$username' AND `active` = '1' "), 0) == 1) ? true : false;
}


# Prüft ob ein Benutzer bereits in der Datenbank vorhanden ist
function user_exists($username) {
    global $link;
    $username = sanitize($username);
    return (mysqli_result(mysqli_query($link, "SELECT COUNT(`user_id`) FROM `users` WHERE `username` = '$username'"), 0) == 1) ? true : false;
}

# Schreibt alle User-Informationen ab
function user_info($user_id) {
    global $link;
    $data = mysqli_fetch_assoc(mysqli_query($link, "SELECT `user_id`, `username`, `active` FROM `users` WHERE `user_id` = '".$user_id."'"));
    return $data;
}

# Erhalte die User-ID anhand des Loginnamens
function user_id_from_username($username) {
    global $link;
    $username = sanitize($username);
    return mysqli_result(mysqli_query($link, "SELECT `user_id` FROM `users` WHERE `username` = '$username'"), 0, 'user_id');
}

# Prüft ob der Benutzer über Admin-Rechte verfügt
function is_admin($user_id) {
    global $link;
    return (mysqli_result(mysqli_query($link, "SELECT `admin` FROM `users` WHERE `user_id` = '$user_id'"), 0) == 1) ? true : false;
}
?>