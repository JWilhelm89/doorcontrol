<img src='img/gitlab/title.png' height='250px'>

# Raumüberwachung / Doorcontrol
Hier wird die Entwicklung des Back- und Front-Ends für das Techniker-Projekt dargestellt.

## Voraussetzungen
- Datenbank mit Schreib-Rechten
- das Verzeichnis "upload" muss vom Webserver (z.B. Apache) beschreibbar sein
- die Datei "core/initial_setup.php" muss vom Webserver beschreibbar sein
- cron-Job für das Python-Skript muss auf dem Server aktiv sein
- Für Uploads muss die exif-Erweiterung auf dem Web-Server ausgeführt sein


# Planung:
## Ziele / Stand
- [x] Benutzerverwaltung (Login-Maske für Bearbeiter)
- [x] Aufteilung in zwei Seiten: Betrachter und Bearbeiter
- [x] Asynchrones Laden der SPS-Daten ([AJAX](https://de.wikipedia.org/wiki/Ajax_(Programmierung)) und [JSON](https://de.wikipedia.org/wiki/JavaScript_Object_Notation) - Gesamtidee --> [Link](https://www.sps-forum.de/threads/php-siemens-plc.32409/post-490998))
- [x] Möglichkeit Grundriss auszutauschen
- [x] Möglichkeit Räume anzulegen
- [x] Möglichkeit Räume zu bearbeiten
- [x] Möglichkeit Räume zu löschen
- [x] Möglichkeit Türen / Tore einzutragen
- [x] Möglichkeit Türen / Tore zu bearbeiten
- [x] Möglichkeit Türen / Tore zu löschen
- [ ] optional: Möglichkeit Temperatur-Grenzwerte festzulegen
- [x] optional: Detail-Ansicht von Räumen (Nah-Ansicht, ggf. mit zusätzlichen Infos)

## Nice to have / Zusatzfunktionen
- [ ] Greifbare Raumgrenzen, um Raumgrenzen mit der Maus zu verschieben
- [ ] Converter .dxf / .dwg --> .png (z.B. per Python), um Grundrisse aus CAD-Zeichnungen hochzuladen (aktuell niedrige Prio, da das CAD Programm auch hochauflösende Bilder generieren kann, welche als Grundriss genutzt werden können)
- [x] Zufallsgenerator von Raumtemperaturen zu Simulationszwecken
- [x] "Darkmode" (schwarzer Hintergrund + invertierter Grundriss - [Lösungsansatz](https://stackoverflow.com/a/13325820))
- [ ] Stummschalten aller Hupen über das Frontend ([Lösungsansatz](https://www.sps-forum.de/threads/php-siemens-plc.32409/post-490737))

## Mögliche Hürden
- ~~siehe [Link 1](https://www.sps-forum.de/threads/php-siemens-plc.32409/post-538106), [Link 2](https://www.sps-forum.de/threads/php-siemens-plc.32409/post-619380)~~

# Planänderung:
## PUT/GET durch OPC abgelöst
- es wurde anstelle der PUT/GET Variante auf Abruf der Daten per OPC-Server/Client umgestiegen. Dies ermöglicht eine sichere, moderne Schnittstelle mit Option der verschlüsselten Datenübertragung.
- Der Abgriff vom OPC-Server erfolgt zyklisch mittels Python-Skript, welches ein JSON-Object liefert

## Ablauf-Schema
```mermaid
graph TB

  SubGraph1 --> SubGraph1Flow
  subgraph "Web-Server"
  SubGraph1Flow(Python)
  SubGraph1Flow -- JSON --> Frontend
  end

  subgraph "SPS"
  Node1[Sensoren] -- IO-Link --> Node2[IO-Link Master]
  Node2 -- ProfiNet --> Node3[SPS]
  Node3 --> SubGraph1[OPC UA Server]
  
end