<?php
include '../core/database/connect.php';

if(isset($_POST['rel']) && isset($_POST['x']) && isset($_POST['y'])){
    /*** Lädt alle Räume aus der Datenbank ***/
    global $link;
    
    $modX=$_POST['rel'];
    $modY=$_POST['rel'];
    $x=$_POST['x'];
    $y=$_POST['y'];
    
    $sql = "SELECT * FROM rooms";
    $result = mysqli_query($link, $sql) or die(mysqli_error($link));
    $result_array = NULL;
    if(mysqli_num_rows($result) >= 1){
        $rooms = array();
        while ($row = mysqli_fetch_object($result)) {
            $id         = $row -> id;
            $title      = $row -> title;
            $name       = $row -> name;
            $temp_adr   = $row -> temp_adr;
            $alarm_adr  = $row -> alarm_adr;
            $geometry   = $row -> geometry;
            $rooms[] = array($id, $title, $name, $temp_adr, $alarm_adr, $geometry);
        }
        $output_arr = array();
        foreach ($rooms as $room){
            if($modX != "" || $modY != ""){
                $string = $room[5];
                $coordinate = explode(", ", $string);
                foreach($coordinate as $axis){
                    $value = explode(",",$axis);
                    $xval = round(($value[0])*($modX)+($x));
                    $yval = round(($value[1])*($modY)+($y));
                    $newcord = $xval.",".$yval;
                    $output[] = $newcord;
                    unset($value);
                }
                unset($coordinate);
                $out = implode(", ", $output); // Achtung Bug-Gefahr 
                unset($output);
                $output_arr[] = array($room[0], $room[1], $room[2], $room[3], $room[4], array($out));
            }else{
                $output_arr[] = array($room[0], $room[1], $room[2], $room[3], $room[4], array($room[5]));
            }
        }
        $result_array = array_values($output_arr);
    }else{
        return FALSE;
    }
    echo json_encode($result_array);
}else{
    return FALSE;
}