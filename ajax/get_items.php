<?php
include '../core/database/connect.php';

if(isset($_POST['rel']) && isset($_POST['x']) && isset($_POST['y'])){
    /*** Lädt alle Türen aus der Datenbank ***/
    global $link;
    
    $modX=$_POST['rel'];
    $modY=$_POST['rel'];
    $x=$_POST['x'];
    $y=$_POST['y'];
    
    $sql = "SELECT * FROM items";
    $result = mysqli_query($link, $sql) or die(mysqli_error($link));
    $result_array = NULL;
    if(mysqli_num_rows($result) >= 1){
        $items = array();
        while ($row = mysqli_fetch_object($result)) {
            $id         = $row -> id;
            $room_id    = $row -> room_id;
            $item_adr   = $row -> item_adr;
            $type       = $row -> type;
            $geometry   = $row -> geometry;
            $rotation   = $row -> rotation;
            $items[] = array($id, $room_id, $type, $item_adr, $geometry, $rotation);
        }
        $output_arr = array();
        foreach ($items as $item){
            if($modX != "" || $modY != ""){
                $string = $item[4];
                $coordinate = explode(", ", $string);
                foreach($coordinate as $axis){
                    $value = explode(",",$axis);
                    $xval = round(($value[0])*($modX)+($x));
                    $yval = round(($value[1])*($modY)+($y));
                    $newcord = $xval.",".$yval;
                    $output[] = $newcord;
                    unset($value);
                }
                unset($coordinate);
                $out = implode(", ", $output);
                unset($output);
                $output_arr[] = array($item[0], $item[1], $item[2], $item[3], $out, $item[5]);
            }else{
                $output_arr[] = array($item[0], $item[1], $item[2], $item[3], $item[4], $item[5]);
            }
        }
        $result_array = array_values($output_arr);
    } else {
        return FALSE;
    }
    echo json_encode($result_array);
}else{
    return FALSE;
}