<?php
include '../core/database/connect.php';

if(isset($_POST['id']) && isset($_POST['rel']) && isset($_POST['x']) && isset($_POST['y'])){
    $id = $_POST['id'];
    
    $modX = $_POST['rel'];
    $modY = $_POST['rel'];
    $x = $_POST['x'];
    $y = $_POST['y'];
    
    
    $query = "SELECT geometry FROM rooms WHERE id = '".$id."'";
    $result = mysqli_query($link, $query);
    $offset = 20;
    if(mysqli_num_rows($result) > 0){
        while ($row = mysqli_fetch_object($result)) {
            $geometry   = $row -> geometry;
        }
        $wertepaar = explode(', ', $geometry);
        $x_werte = array();
        $y_werte = array();
        foreach($wertepaar as $coordinate){
            $werte = explode(',',$coordinate);
            $x_werte[] = intval($werte[0]);
            $y_werte[] = intval($werte[1]);
        }
        if($modX != "" || $modY != ""){
            $min_x = round(min($x_werte)*($modX)+($x))-$offset;
            $min_y = round(min($y_werte)*($modY)+($y))-$offset;
            $max_x = round(max($x_werte)*($modX)+($x))+$offset;
            $max_y = round(max($y_werte)*($modY)+($y))+$offset;
        }else{
            $min_x = min($x_werte)-$offset;
            $min_y = min($y_werte)-$offset;
            $max_x = max($x_werte)+$offset;
            $max_y = max($y_werte)+$offset;
        }
        $width = $max_x - $min_x;
        $height = $max_y - $min_y;
        $result = $min_x." ".$min_y." ".$width." ".$height;
    }
    echo json_encode($result);
}
?>