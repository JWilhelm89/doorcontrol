<?php
require 'core/init.php';
logged_in_redirect();
?>
<!DOCTYPE html>
<html lang="de">
    <head>
        <link rel="stylesheet" type="text/css" href="css/screen.css">
    </head><body>
<?php
global $link;
if(empty($_POST) === false){
    $required_fields = array('loginname', 'password', 'password_again');
    foreach ($_POST as $key => $value){
        if(empty($value) && in_array($key, $required_fields) === TRUE){
            $errors[] = "Es wurden nicht alle Pflichtfelder ausgefüllt.";
            break 1;
        }
    }
    # Weitere Überprüfungen
    if(empty($errors) === TRUE){
        # Überprüfung ob Login-Name bereits in Verwendung ist
        if(user_exists($_POST['loginname']) === TRUE){
            $errors[] = "Der Name '" . $_POST['loginname'] . "' wird bereits verwendet. Bitte wähle einen anderen Namen.";
        }
        
        # Überprüfung auf (unzulässige) Leerzeichen
        if(preg_match("/\\s/", $_POST['loginname']) === TRUE){
            $errors[] = "Der Name darf keine Leerzeichen beinhalten.";
        }
        
        # Überprüfung auf übereinstimmende Passwörter
        if($_POST['password'] !== $_POST['password_again']){
            $errors[] = "Die Passwörter sind nicht identisch.";
        }
    }
}
if(isset($_GET['success']) && empty($_GET['success'])){
    echo "<div style='text-align: center;'>Sie wurden erfolgreich registriert.<br>Bitte warten Sie noch auf die Freischaltung durch den Administrator der Seite.";
    echo "<a href='javascript:window.close()'>Fenster schließen</a></div>";
}else{
    if(empty($_POST) === FALSE && empty($errors) === TRUE){
        $register_data = array(
            'username' => ucfirst($_POST['loginname']),
            'password' => $_POST['password']
        );
        register_user($register_data);
        header('Location: register.php?success');
        exit();
    }elseif (empty($errors) === FALSE){
        # Fehlerausgabe
        echo output_errors($errors);
    }
?>
        <!-- Formular zur Registrierung -->
        <form id='regform' name='reg' method='POST'>
            <table>
                <tr><td>Login-Name:</td><td><input type='text' name='loginname'></td></tr>
                <tr><td>Passwort:</td><td><input type='password' name='password'></td></tr>
                <tr><td>Passwort Wiederholung:</td><td><input type='password' name='password_again'></td></tr>
                <tr><td colspan='2'><input type='submit' value='registrieren'></td></tr>
            </table>
        </form>
<?php
}
?>
    </body>
</html>